/**
 *  Read/write tests of the ESP8622 EEPROM
 *   Uses struct to store values
 *   Goal to build up menu for setting values
 * 
 *  @author Aaron S. Crandall <acrandal@gmail.com>, 2020
 *  @copyright 2020
 */


#include <EEPROM.h>
#include <ESP8266WiFi.h>

#define EEPROM_ALLOCATE 256
#define EEPROM_CONFIGS_ADDR 0

struct Configs {
  char ssid[30] = "wifi ssid";
  char password[30] = "wifi password";
  char mqtt_server[30] = "127.0.0.1";
  char device_location[30] = "unknown";
};

Configs g_configs;



bool g_light_on = false;

// ** Flip the LED state ******************************************
void toggle_light() {
  if( g_light_on ) {
    digitalWrite(BUILTIN_LED, HIGH);
    g_light_on = false;
  } else {
    digitalWrite(BUILTIN_LED, LOW);
    g_light_on = true;
  }
}

// ** Editor prompt **********************************************************
void show_configs_editor_prompt() {
  Serial.println(" ******** Current configuration:");
  display_configs();
  Serial.println("(s) Save Configuration to EEPROM");
  Serial.println("(q) Quit configuration (no save)");
  Serial.println();
  Serial.println(" Enter choice: ");
}


// ** Get a user's input, with a pause ***************************************
String get_user_menu_selection(int secs_pause) {
  String user_entry = "";
  int timeout_count = 0;
  while(user_entry == "" && (secs_pause == 0 || timeout_count < secs_pause) ) {
    user_entry = Serial.readString();
    user_entry.trim();
    timeout_count++;
  }
  return user_entry;
}

void edit_one_config(char* config_string) {
  Serial.print("Current value: ");
  Serial.println(config_string);
  Serial.print("Enter new config value: ");
  String user_selection = get_user_menu_selection(0);
  user_selection.toCharArray(config_string, 30);
  Serial.print("Config updated to: ");
  Serial.println(config_string);
}

// ** Configs EDIT menu ******************************************************
void do_configs_editor() {
  bool done = false;

  while( !done ) {
    show_configs_editor_prompt();
    String user_selection = get_user_menu_selection(0);

    Serial.print("User entered: ");
    Serial.println(user_selection);

    if(user_selection.length() < 1) { return; }

    char* string_to_edit;
    switch (tolower(user_selection[0])) {
      case '1':
        Serial.println("Editing SSID.");
        string_to_edit = g_configs.ssid;
        edit_one_config(string_to_edit);
        break;
      case '2':
        Serial.println("Editing Wifi Password.");
        string_to_edit = g_configs.password;
        edit_one_config(string_to_edit);
        break;
      case '3':
        Serial.println("Editing MQTT Server IP.");
        string_to_edit = g_configs.mqtt_server;
        edit_one_config(string_to_edit);
        break;
      case '4':
        Serial.println("Editing Device location string.");
        string_to_edit = g_configs.device_location;
        edit_one_config(string_to_edit);
        break;
      case 's':
        Serial.print("Saving to EEPROM -- ");
        save_current_config();
        Serial.println("Save complete.");
        break;
      case 'q':
        Serial.println("Quitting to normal mode");
        done = true;
        break;
    }
  }
}


// ** Pause if need to edit by user ******************************************
void do_pause_for_user_menu() {
  String serial_input = "";

  for(int i = 0; i < 4; i++) {
    Serial.println("*");
  }
  Serial.println("************************************************");
  Serial.println("** Welcome to Crandall's House IoT Device     **");
  Serial.println("************************************************");
  Serial.println("");
  Serial.println("Enter anything + enter for configuration editing menu");
  Serial.println(" --> Dots are counting a 15 second timeout to start normally");
  serial_input = Serial.readString();

  for( int i = 0; i < 15; i++ ) {   // Waits 15 seconds for user interrupt
    if( serial_input != "" ) { break; }
    Serial.print(".");
    serial_input = Serial.readString();
  }
  Serial.println("");

  if(serial_input != "") {
    // Change to function call
    
    Serial.print("Entered string: ");
    Serial.print(serial_input);
    Serial.println(" --> Entering configs editor");
    do_configs_editor();
  } else {
    Serial.println("No user interrupt, beginning normal operations");
  }


}

// ** Initialize hardware systems *********************************
void init_hardware() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  
  Serial.begin(115200);             // Init serial
  while(!Serial) { delay(100); }
  delay(1000);                      // Give serial time to settle

  EEPROM.begin(EEPROM_ALLOCATE);          // Allocate EEPROM for use (max 512b)
  Serial.println("\nHardware initialized.");
}


// ** Load in the current configuration from EEPROM *************************************
void load_current_config() {
  EEPROM.get(EEPROM_CONFIGS_ADDR, g_configs);
}


// ** Save current config to EEPROM *****************************************************
void save_current_config() {
  EEPROM.put(EEPROM_CONFIGS_ADDR, g_configs);
  EEPROM.commit();
}

// ** Display summary of current config *************************************************
void display_configs() {
  Serial.println("********************* Current configs *********************");
  Serial.print("(1) SSID: ");
  Serial.println(g_configs.ssid);
  Serial.print("(2) Pass: ");
  Serial.println(g_configs.password);
  Serial.print("(3) MQTT Server: ");
  Serial.println(g_configs.mqtt_server);
  Serial.print("(4) Device Location: ");
  Serial.println(g_configs.device_location);
}


// ** One off setup at boot **************************************************************
void setup() {
  init_hardware();

  load_current_config();
  display_configs();

  do_pause_for_user_menu();
  display_configs();

  //strncpy(g_configs.ssid,"new ssid", 30);
  //Serial.println(g_configs.ssid);

  // ** Start main operations
  Serial.println("Setup complete, beginning normal operations -->");
}


// ** Repeated loop called when loop ends *************************************************
void loop() {
  toggle_light();
  delay(1000);
}
