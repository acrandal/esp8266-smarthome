#!/usr/bin/env python3

from influxdb import InfluxDBClient

json_body = [
    {
        "measurement": "cpu_load_short",
        "tags": {
            "host": "server01",
            "region": "us-west"
        },
        "time": "2010-11-10T23:00:00Z",
        "fields": {
            "value": 0.74
        }
    }
]

sample = {
    "measurement": "cpu_load_short",
    "tags": {
        "host": "server02",
        "region": "ca-east"
    },
    "fields": {
        "value": 1.30
    }
}

points = [sample]


if __name__ == "__main__":
    print('Hi')

    print(json_body)
    print("*****************")
    print(sample)
    print("*****************")
    print(points)

    #client = InfluxDBClient('bug.local', 8086, 'root', 'root', 'example')
    client = InfluxDBClient(host='bug.local', port=8086, database='example')
    client.create_database('example')
    client.write_points(points)
    result = client.query('select value from cpu_load_short;')
    print("Result: {0}".format(result))
    print("End")